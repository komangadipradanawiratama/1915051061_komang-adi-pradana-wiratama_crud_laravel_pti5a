<!DOCTYPE html>
<html>
    <head>
        <title>Laravel CRUD</title>
    </head>

    <body>
        <h3>Edit Mahasiswa</h3>
        <a href="/mahasiswa"> Kembali</a>
        <br/>
        <br/>
        
        @foreach($mahasiswa as $s)
        <form action="/mahasiswa/update" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $s->id }}"> 
            <br/>
            Nama <input type="text" required="required" name="nama" value="{{ $s->nama_mahasiswa }}"> 
            <br/>
            Nim <input type="text" required="required" name="nim" value="{{ $s->nim_mahasiswa }}"> 
            <br/>
            Kelas <input type="text" required="required" name="kelas" value="{{ $s->kelas_mahasiswa }}"> 
            <br/>
            Prodi <input type="text" required="required" name="prodi" value="{{ $s->prodi_mahasiswa}}"> 
            <br/>
            Fakultas <input type="text" required="required" name="fakultas" value="{{ $s->fakultas_mahasiswa }}"> 
            <br/>
            <input type="submit" value="Simpan Data">

        </form>
        
        @endforeach
    </body>
</html>