<!DOCTYPE html> 
<html> 
    <head> 
        <title>Laravel CRUD</title> 
    </head> 
    
    <body> 
        <h3>Data Mahasiswa</h3> 
        <a href="/mahasiswa"> Kembali</a> 
        <br/> 
        <br/> 
        
        <form action="/mahasiswa/store" method="post"> 
            {{ csrf_field() }} 
            Nama<input type="text" name="nama" required="required"> 
            <br /> 
            Nim<input type="number" name="nim" required="required"> 
            <br /> 
            Kelas<input type="text" name="kelas" required="required"> 
            <br /> 
            Prodi<input type="text" name="prodi"required="required"> 
            <br /> 
            Fakultas<input type="text" name="fakultas" required="required"> 
            <br /> 
            <input type="submit" value="Simpan Data"> 
        </form> 
    </body> 
</html>