<!DOCTYPE html>
<html>
    <head>
        <title>Laravel CRUD</title>
    </head>

    <body>
        <h3>Data Mahasiswa</h3>

        <a href="/mahasiswa/tambah"> + Tambah Mahasiswa Baru +</a>

        <br/>
        <br/>

        <table border="1">
            <tr>
                <th>Nama</th>
                <th>Nim</th>
                <th>Kelas</th>
                <th>Prodi</th>
                <th>Fakultas</th>
                <th>Aksi</th>
            </tr>

            @foreach($mahasiswa as $p)
            <tr>
                <td>{{ $p->nama_mahasiswa }}</td>
                <td>{{ $p->nim_mahasiswa }}</td>
                <td>{{ $p->kelas_mahasiswa }}</td>
                <td>{{ $p->prodi_mahasiswa }}</td>
                <td>{{ $p->fakultas_mahasiswa}}</td>
                <td>
                    <a href="/mahasiswa/edit/{{ $p->id }}">Edit</a>
                    |
                    <a href="/mahasiswa/hapus/{{ $p->id }}">Hapus</a>
                </td>
            </tr>
            @endforeach
        </table>
    </body>
</html>